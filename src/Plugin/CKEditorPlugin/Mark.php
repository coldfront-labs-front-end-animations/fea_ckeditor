<?php

namespace Drupal\fea_ckeditor\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Mark" plugin.
 *
 * @CKEditorPlugin(
 *   id = "fea_ckeditor_mark",
 *   label = @Translation("Mark"),
 *   module = "fea_ckeditor"
 * )
 */
class Mark extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'fea_ckeditor') . '/js/plugins/mark/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $module_path = drupal_get_path('module', 'fea_ckeditor');
    return [
      'mark' => [
        'label' => $this->t('Mark'),
        'image' => $module_path . '/js/plugins/mark/icons/mark.png',
      ],
    ];
  }

}

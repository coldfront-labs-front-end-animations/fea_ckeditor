/**
 * @file
 * Mark CKEditor plugin.
 *
 * Basic plugin inserting abbreviation elements into the CKEditor editing area.
 *
 * @DCG The code is based on an example from CKEditor Plugin SDK tutorial.
 *
 * @see http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_1
 */

(function (Drupal) {

  'use strict';

  CKEDITOR.plugins.add('fea_ckeditor_mark', {

    // Register the icons.
    icons: 'mark',

    // The plugin initialization logic goes inside this method.
    init: function (editor) {

      // Define an editor command that opens our dialog window.
      editor.addCommand('mark', {
        exec: (editor) => {
          editor.insertHtml(`<mark>${editor.getSelection().getSelectedText()}</mark>`)
        }
      });

      // Create a toolbar button that executes the above command.
      editor.ui.addButton('mark', {

        // The text part of the button (if available) and the tooltip.
        label: Drupal.t('Mark text'),

        // The command to execute on click.
        command: 'mark',

        // The button placement in the toolbar (toolbar group name).
        toolbar: 'insert'
      });
    }
  });

} (Drupal));
